<?php

/**
 *
 * @param type $url
 * @param type $query
 * @param type $post
 */
function requestCurl($url, $query, $post = [], $headers = [])
{
    return @json_decode(requestCurlString($url, $query, $post, $headers), true);
}

function requestCurlString($url, $query = [], $post = [], $headers = [])
{
    if (!empty($query)) {
        $url .= (strpos($url, '?') !== false ? '&' : '?') . http_build_query($query);
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    if (!empty($post)) {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if (defined('API_KEY') && !isset($headers['API_KEY'])) {
        $headers['Api-Key'] = API_KEY;
    }

    if (!empty($_COOKIE)) {
        $headers['Cookie'] = [];
        foreach ($_COOKIE as $name => $val) {
            $headers['Cookie'][] = $name . "=" . $val;
        }
        $headers['Cookie'] = implode(';', $headers['Cookie']);
    }

    if (!empty($headers)) {
        $hh = [];
        foreach ($headers as $name => $value) {
            $hh[] = "$name: $value";
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $hh);
    }

    if (isset($_COOKIE['PHPSESSID'])) {
        curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']);
    }

    $str = curl_exec($ch);
    curl_close ($ch);
    return $str;
}

class ApiCall
{
    protected $callbacks = [];
    protected $get       = [];
    protected $post      = [];
    protected $executed  = false;
    protected $apiKey    = null;

    public function __construct($action = null, array $get = [], $callback = null, $apiKey = null)
    {
        if ($action) {
            call_user_func_array([$this, 'addGet'], func_get_args());
        }

        $this->apiKey = $apiKey;
        if (!$this->apiKey && defined('API_KEY')) {
            $this->apiKey = API_KEY;
        }
    }

    /**
     *
     * @param type $callback
     * @param type $action
     * @param array $get
     */
    public function addGet($action, array $get = [], $callback = null)
    {
        $this->get[$action]       = $get;
        $this->callbacks[$action] = $callback;
        return $this;
    }

    /**
     *
     * @param type $callback
     * @param type $action
     * @param array $post
     */
    public function addPost($action, array $post = [], $callback = null)
    {
        $this->post[$action]       = $post;
        $this->callbacks[$action]  = $callback;
        return $this;
    }

    public function dispatch()
    {
        if ($this->executed) {
            throw new Exception('Request already executed.');
        }
        $this->executed = true;

        if (count($this->callbacks) > 1) {
            $get            = $this->get;
            $get['action']  = implode(',', array_keys($this->callbacks));
            $post           = $this->post;
        } else {
            $action = key($this->callbacks);
            $get    = $this->get[$action] ?? [];
            $get['action'] = $action;
            $post   = $this->post[$action] ?? [];

        }

        $result = requestCurlString(API_URI . '/', $get, $post, ['Api-Key' => (defined('API_KEY') ? API_KEY : '')]);
        $array  = @json_decode($result, true);
        if (!is_array($array)) {
            error_log(json_encode(['request' => [API_URI . '/', $get, $post], 'result' => $result]));
            $array = [];
            throw new Exception($result);
        }

        if (count($this->callbacks) == 1) {
            $array = [$action => $array];
        }

        foreach ($this->callbacks as $action => $callback) {
            if ($callback) {
                call_user_func_array($callback, [$result[$action]]);
            }
        }

        return $array;
    }
}