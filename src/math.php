<?php

/**
 * 0.000021521
 * 0.000021 = floordec(0.000021521, 6);
 *
 * @param type $zahl
 * @param type $decimals
 * @return type
 */
function floordec($zahl, $decimals = 2)
{
    return floor($zahl * pow(10, $decimals)) / pow(10, $decimals);
}

/**
 * 0.00000521
 * 521 = coinsToPoints(0.00000521)
 * @param type $coins
 * @return type
 */
function coinsToPoints($coins)
{
    $coins = str_replace(
        '.',
        '',
        number_format(
            $coins,
            8,
            '.',
            ''
        )
    );
    return (int) ltrim($coins, '0');
}

/**
 * 521
 * 0.00000521 = pointsToCoins(521);
 * @param string $points
 * @return type
 */
function pointsToCoins($points)
{
    $points = '' . $points;
    $coins = str_pad($points, 8, '0', STR_PAD_LEFT);

    return sprintf(
        '%s.%s',
        strlen($coins) > 8 ? substr($coins, 0, -8) : 0,
        substr($coins, -8)
    );
}

/**
 * Check if conversion is needed
 * @param type $points
 * @return type
 */
function pointsToCoinsIf($points)
{
    if (('' . intval($points)) === ('' . $points)) {
        return pointsToCoins($points);
    }
    return number_format(floatval($points), 8, '.', '');
}

/**
 *  1000    = 1k
 *  10000   = 10k
 *  1000000 = 100k
 *
 *
 * @param type $number
 */
function numberShort($number, $precision = 0)
{
    // Setup default $divisors if not provided
    $divisors = array(
        pow(1000, 0) => '', // 1000^0 == 1
        pow(1000, 1) => 'K', // Thousand
        pow(1000, 2) => 'M', // Million
        pow(1000, 3) => 'B', // Billion
        pow(1000, 4) => 'T', // Trillion
        pow(1000, 5) => 'Qa', // Quadrillion
        pow(1000, 6) => 'Qi', // Quintillion
    );

    // Loop through each $divisor and find the
    // lowest amount that matches
    foreach ($divisors as $divisor => $shorthand) {
        if (abs($number) < ($divisor * 1000)) {
            // We found a match!
            break;
        }
    }

    // We found our match, or there were no matches.
    // Either way, use the last defined value for $divisor.
    return number_format($number / $divisor, $precision) . $shorthand;
}

/**
 *
 * @param type $seconds
 * @return type
 */
function secondsShort($seconds)
{
    if ($seconds < 60) {
        return $seconds . 's';
    } elseif ($seconds < 60 * 60) {
        return floor($seconds / 60) . 'm';
    } elseif ($seconds < 60 * 60 * 24) {
        return floor($seconds / 60 / 60) . 'h';
    } elseif ($seconds < 60 * 60 * 24 * 7) {
        return floor($seconds / 60 / 60 / 24) . 'd';
    } elseif ($seconds < 60 * 60 * 24 * 7 * 30) {
        return floor($seconds / 60 / 60 / 24 / 7) . 'w';
    } elseif ($seconds < 60 * 60 * 24 * 7 * 30 * 12) {
        return floor($seconds / 60 / 60 / 24 / 7 / 30) . 'm';
    } else/* ($seconds < 60 * 60 * 24 * 7 * 30 * 12 * 100)*/ {
        return floor($seconds / 60 / 60 / 24 / 7 / 30 / 12) . 'y';
    }
    return $seconds;
}
