<?php

/**
 * @TODO MAKE PROFILER
 */
class PdoProfiler extends \PDO
{
    protected $logs = [];

    public function query($query)
    {
        $start  = microtime(true);
        $result = parent::query($query);
        $this->logs[] = [
            'time'  => microtime(true) - $start,
            'query' => $query,
        ];
        return $result;
    }

    public function getLogs()
    {
        return $logs;
    }
}