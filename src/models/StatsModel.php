<?php

class StatsModel
{
    protected $dir;
    protected $get;
    protected $pdo;
    protected $outgoingBytes  = 0;
    protected $incommingBytes = 0;
    protected $time           = 0;

    public function __construct(PdoProfiler $pdo, $dir)
    {
        $this->get = $_GET;
        $this->pdo = $pdo;
        $this->dir = $dir;

        ob_start();
    }

    public function capture()
    {
        register_shutdown_function([$this, 'captureStore']);
    }

    public function captureStore()
    {
        $string               = ob_get_clean();
        $this->outgoingBytes  = strlen($string);
        $this->incommingBytes = strlen(file_get_contents("php://input"));
        $this->time           = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];

        $data = [
            'date'              => time(),
            'uri'               => $_SERVER['REQUEST_URI'],
            'outgoingBytes'     => $this->outgoingBytes,
            'incommingBytes'    => $this->incommingBytes,
            'time'              => $this->time,
        ];

        $file = $this->dir.'/'.date('YmdH').'.php';
        @file_put_contents($file, json_encode($data).PHP_EOL, FILE_APPEND);

        echo $string;
    }
}