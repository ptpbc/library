<?php

class EmailMaker
{
    protected $dir;
    protected $template;
    protected $subject;
    protected $params;
    protected $from;
    protected $reply;
    protected $html;
    protected $text;
    protected $priority = 50;
    protected $unsubscribe = false;
    
    public function __construct($template, $subject, $params, $from = 'Noreply ptpbc <noreply@ptpbc.com>', $reply = 'Noreply ptpbc <noreply@ptpbc.com>')
    {
        $this->dir      = __DIR__ . '/template';
        $this->template = $template;
        $this->subject  = $subject;
        $this->params   = $params;
        $this->from     = $from;
        $this->reply    = $reply;
    }

    public function __get($name)
    {
        return $this->params[$name] ?? null;
    }

    public function setPriority($int)
    {
        $this->priority = $int;
    }

    public function setUnsubscribe($bool)
    {
        $this->unsubscribe = (bool) $bool;
    }

    public function save($recipients)
    {
        $this->createTemplate();

        $emailTo = [];
        foreach ($recipients as $email) {
            $emailTo[] = ['email' => $email];
        }

        $post = [
            'email_from' => $this->from,
            'email_reply' => $this->reply,
            'email_subject' => $this->subject,
            'email_message_text' => $this->text,
            'email_message_html' => $this->html,
            'email_to' => $emailTo,
        ];

        if (defined('API_URI') && class_exists('ApiCall')) {
            $api = new \ApiCall;
            $api->addPost('addEmail', $post);
            $result = $api->dispatch();
        } elseif (function_exists('addEmail')) {
            global $pdo, $config;
            $response = [];
            addEmail($pdo, $response, [], $post, $config);
        } else {
            throw new Exception('Can not save email');
        }
    }
    
    public function createTemplate()
    {
        $this->html = $this->parseTemplate($this->dir . '/' . $this->template . '.html.phtml', $this->params);
        $this->html = nl2br($this->html);
        $this->text = $this->parseTemplate($this->dir . '/' . $this->template . '.text.phtml', $this->params);

        if ($this->unsubscribe) {
            $this->text .= sprintf(
                '%s%s%s',
                PHP_EOL,
                PHP_EOL,
                'Manage email lists and unsubscribe: https://ptpbc.com/user/profile/emails'
            );
            $this->html .= PHP_EOL . sprintf(
                '%s%s%s',
                '<br/>',
                '<br/>',
                '<a href="https://ptpbc.com/user/profile/emails" target="_blank">Manage email lists and unsubscribe</a>'
            );
        }

    }

    public function parseTemplate($file, $params)
    {
        ob_start();
        include $file;
        return ob_get_clean();
    }
}