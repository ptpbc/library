<?php

/**
 *
 * @param type $required
 * @param type $params
 * @param type $error
 */
function validate($required, $params, &$error)
{
    $error = [];
    $values = [];
    foreach ($required as $name) {
        if (!array_key_exists($name, $params)) {
            $error[] = '$_POST[`' . $name . '`] is required';
        } else {
            $values[$name] = $params[$name];
        }
    }
    return $values;
}

/**
 * true = isStringCoin(0.00000521);
 * @param type $str
 * @return type
 */
function isStringCoin($str)
{
    return substr('' . $str, -9, 1) === '.';
}