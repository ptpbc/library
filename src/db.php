<?php

/**
 * @param PDO $pdo
 * @param type $table
 * @param type $pair [id => 1]
 */
function dbRow(PDO $pdo, $table, array $pair, $order = null, $columns = null)
{
    $rows = dbRows($pdo, $table, $pair, $order, $columns, 1);
    return isset($rows[0]) ? $rows[0] : false;
}

/**
 *
 * @param PDO $pdo
 * @param type $table
 * @param array $pair
 * @param type $order
 * @param type $limit
 * @return type
 */
function dbRows(PDO $pdo, $table, array $pair = [], $order = null, $columns = null, $limit = null)
{
    dbWhere($pair, $where, $values);

    $columnsPair = [];
    if ($columns) {
        foreach ($columns as $name => $as) {
            $columnsPair[] = "`".str_replace('.', '`.`', (is_numeric($name) ? $as : $name))."` AS `$as`";
        }
    } else {
        $columnsPair[] = '*';
    }

    if (is_array($table)) {
        $from       = null;
        $lastTable  = null;
        $lastPk     = null;
        foreach ($table as $name => $pk) {
            if (!$from) {
                $from = "FROM `$name`";
            } else {
                $from .= " LEFT JOIN `$name` ON `$lastTable`.$lastPk = `$name`.$pk";
            }
            $lastPk     = $pk;
            $lastTable  = $name;
        }
    } else {
        $from = "FROM `$table`";
    }
    $stmt = $pdo->prepare(
        "SELECT " . implode(',' , $columnsPair) . " "
        . " $from "
        . (empty($where) ? '' : "WHERE " . implode(' AND ', $where))
        . ($order ? ' ORDER BY ' . $order : '')
        . ($limit ? ' LIMIT ' . $limit : '')
    );
    $stmt->execute($values);
    $result = $stmt->fetchAll($pdo::FETCH_ASSOC);
    unset($stmt);
    return $result;
}

/**
 *
 * @param PDO $pdo
 * @param type $table
 * @param type $params
 * @return type
 */
function dbInsertDublicateUpdate(PDO $pdo, $table, $params)
{
    $columns = [];
    $binds   = [];
    $values  = [];
    $updates = [];

    $multipleValues = [];
    if (!is_numeric(key($params))) {
        $params = [$params];
    }

    foreach ($params as $num => $subParams) {
        foreach ($subParams as $name => $value) {
            if (!isset($updates[$name])) {
                $updates[$name] = "`$name`=VALUES(`$name`)";
            }
            if ($value === 'NOW()' || strpos($value, 'DATE_FORMAT') !== false) {
                $columns[$name]             = '`'.$name.'`';
                $binds[]                    = $value;
            } else {
                $columns[$name]             = '`'.$name.'`';
                $binds[]                    = ':'. $num . $name;
                $values[':' . $num . $name] = $value;
            }
        }

        $multipleValues[] = "(" . implode(',', $binds) . ")";
        $binds = [];
    }

    $stmt = $pdo->prepare(
        "INSERT INTO `$table` (".implode(',', $columns).")"
        ." VALUES ".implode(',', $multipleValues).""
        ." ON DUPLICATE KEY UPDATE ".implode(',', $updates)
    );
    $stmt->execute($values);
    $result = (int) $pdo->lastInsertId();
    unset($stmt);
    return $result;
}

/**
 *
 * @param PDO $pdo
 * @param type $table
 * @param type $params
 * @return type
 */
function dbInsert(PDO $pdo, $table, $params, $ignore = false)
{
    $columns = [];
    $binds   = [];
    $values  = [];

    $multipleValues = [];
    if (!is_numeric(key($params))) {
        $params = [$params];
    }

    foreach ($params as $num => $subParams) {
        foreach ($subParams as $name => $value) {
            if ($value === 'NOW()' || strpos($value, 'DATE_FORMAT') !== false) {
                $columns[$name]             = '`'.$name.'`';
                $binds[]                    = $value;
            } else {
                $columns[$name]             = '`'.$name.'`';
                $binds[]                    = ':'. $num . $name;
                $values[':' . $num . $name] = $value;
            }
        }

        $multipleValues[] = "(" . implode(',', $binds) . ")";
        $binds = [];
    }


    $ignore = $ignore === true ? 'IGNORE' : '';
    $stmt = $pdo->prepare(
        "INSERT $ignore INTO `$table` (".implode(',', $columns).")"
        ." VALUES " .implode(',', $multipleValues)
    );
    $stmt->execute($values);
    $result = (int) $pdo->lastInsertId();
    unset($stmt);
    return $result;
}

/**
 *
 * @param PDO $pdo
 * @param type $table
 * @param array $params
 * @param array $where
 */
function dbUpdate(PDO $pdo, $table, array $params, array $where)
{
    $set     = [];
    $values  = [];
    dbWhere($where, $andWhere, $values);

    foreach ($params as $name => $value) {
        if ($value === 'NULL') {
            $set[]           = "`$name` = NULL";
        } else if ($value === 'NOW()') {
            $set[]           = "`$name` = NOW()";
        } else {
            $set[]           = "`$name` = :$name";
            $values[':'.$name] = $value;
        }
    }

    $stmt = $pdo->prepare(
        "UPDATE `$table` SET " . implode(',', $set) . " WHERE " . implode(' AND ', $andWhere)
    );
    $stmt->execute($values);
    unset($stmt);
}

/**
 *
 * @param PDO $pdo
 * @param type $table
 * @param array $params
 * @param array $where
 */
function dbDelete(PDO $pdo, $table, array $where)
{
    dbWhere($where, $andWhere, $values);
    $stmt = $pdo->prepare(
        "DELETE FROM `$table` WHERE " . implode(' AND ', $andWhere)
    );
    $stmt->execute($values);
    unset($stmt);
}

/**
 *
 * @param array $where
 * @param type $bind
 * @param type $values
 */
function dbWhere(array $where, &$bind, &$values)
{
    $bind = [];
    $values = [];
    foreach ($where as $name => $value) {
        if (is_array($value)) {
            $ins = [];
            $key = str_replace('.', '_', $name);
            foreach ($value as $k => $in) {
                $ins[] = ':' . $key . '_b_'. $k;
                $values[':' . $key . '_b_'. $k] = $in;
            }
            $bind[]             = "`".str_replace('.', '`.`', $name)."` IN(".implode(',', $ins).")";
        } else if (is_numeric($name) && is_string($value)) {
            $bind[] = $value;
        } else {
            $key                = str_replace('.', '_', $name);
            $bind[]             = "`".str_replace('.', '`.`', $name)."` = :$key";
            $values[':' . $key] = $value;
        }
    }
}


function getMysqlDate()
{
    static $date;
    if ($date) {
        return $date;
    }

    global $pdo;

    $statement = $pdo->prepare('SELECT NOW() AS date');
    $statement->execute();
    $result = $statement->fetch();
    return $date = $result['date'];
}