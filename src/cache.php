<?php
/**
 *
 * @param type $key
 * @param type $data
 * @return type
 */
function cache($key, $data = null)
{
    $file = CACHE_DIR . '/' . sha1($key) . '.serialize';
    if ($data === null) {
        if (file_exists($file)) {
            return unserialize(file_get_contents($file));
        } else {
            return null;
        }
    } else {
        file_put_contents($file, serialize($data));
        return $data;
    }
}